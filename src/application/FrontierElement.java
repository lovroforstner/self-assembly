package application;

public class FrontierElement {
    private final Coordinate tileAndGlue;
    private final Coordinate tileSetOffset;
    private final TileSetTile tileSetTile;

    public FrontierElement(Coordinate tg, Coordinate tOffset, TileSetTile t) {
        tileAndGlue = tg;
        tileSetOffset = tOffset;
        tileSetTile = t;
    }

    public Coordinate getLocation() {
        return tileAndGlue;
    }

    public Coordinate getOffset() {
        return tileSetOffset;
    }

    public TileSetTile getTileSetTile() {
        return tileSetTile;
    }




}
