package application;
import application.configuration.TileConfiguration;
import javafx.util.Pair;
import java.util.*;

public class Assembly {
    private static final int NORTH = 0;
    private static final int EAST = 1;
    private static final int SOUTH = 2;
    private static final int WEST = 3;
    public final HashMap<Coordinate, Tile> Grid = new HashMap<>();

    private final
    HashMap<Coordinate, String> openNorthGlues = new HashMap<>();
    private final
    HashMap<Coordinate, String> openEastGlues = new HashMap<>();
    private final
    HashMap<Coordinate, String> openSouthGlues = new HashMap<>();
    private final
    HashMap<Coordinate, String> openWestGlues = new HashMap<>();
    private final ArrayList<FrontierElement> possibleAttach = new ArrayList<>();
    private final Frontier frontier;
    public TileConfiguration tileConfiguration;
    public int numberOfTiles;

    public Assembly() {
        tileConfiguration = new TileConfiguration(2);
        frontier = new Frontier();
    }

    public Assembly(TileConfiguration tc) {
        tileConfiguration = new TileConfiguration(tc.getTemperature());
        tileConfiguration.getGlueFunction().putAll(tc.getGlueFunction());
        tileConfiguration.getTileTypes().addAll(tc.getTileTypes());
        frontier = new Frontier();
    }

    public void getOpenGlues() {
        openNorthGlues.clear();
        openEastGlues.clear();
        openSouthGlues.clear();
        openWestGlues.clear();
        for (Map.Entry<Coordinate, Tile> t : Grid.entrySet()) {
            String[] glueLabels = t.getValue().getGlueLabels();
            Coordinate spot = t.getKey();

            if (glueLabels[0] != null && Grid.get(spot.translate(0, 1)) == null) {
                openNorthGlues.put(t.getKey(), glueLabels[0]);
            }
            if (glueLabels[1] != null && Grid.get(spot.translate(1, 0)) == null) {
                openEastGlues.put(t.getKey(), glueLabels[1]);
            }
            if (glueLabels[2] != null && Grid.get(spot.translate(0, -1)) == null) {
                openSouthGlues.put(t.getKey(), glueLabels[2]);
            }
            if (glueLabels[3] != null && Grid.get(spot.translate(-1, 0)) == null) {
                openWestGlues.put(t.getKey(), glueLabels[3]);
            }
        }
    }

    public void placeTileSetTile(TileSetTile p, int x, int y) {
        for (Tile t : p.tiles) {
            Coordinate tmp = new Coordinate(t.getLocation());
            tmp = tmp.translate(x, y);
            Grid.put(tmp, t);
        }
    }

    public boolean checkGeometry(TileSetTile p, int x, int y) {
        for (Tile t : p.tiles) {
            if (Grid.containsKey(new Coordinate(x + t.getLocation().getX(), y + t.getLocation().getY()))) {
                return false;
            }
        }
        return true;
    }

    private Pair<Coordinate, Coordinate> getOffset(Coordinate aPoint, Coordinate ptPoint, int offsetX, int offsetY) {
        Coordinate placement = new Coordinate(offsetX, offsetY);
        placement = placement.translate(aPoint.getX(), aPoint.getY());
        int xOffset = -(ptPoint.getX() - placement.getX());
        int yOffset = -(ptPoint.getY() - placement.getY());
        Coordinate tmp2 = new Coordinate(xOffset, yOffset);
        return new Pair<>(placement, tmp2);
    }

    public void checkMatchingGlues(TileSetTile t) {
        fillPossibleList(t, NORTH);
        fillPossibleList(t, EAST);
        fillPossibleList(t, SOUTH);
        fillPossibleList(t, WEST);
    }

    private void fillPossibleList(TileSetTile tst, int direction) {
        HashMap<Coordinate, String> tstGlues;
        HashMap<Coordinate, String> glues;
        int offsetX;
        int offsetY;

        switch (direction) {
            case NORTH:
                tstGlues = tst.southGlues;
                glues = openNorthGlues;
                offsetX = 0;
                offsetY = 1;
                break;
            case EAST:
                tstGlues = tst.westGlues;
                glues = openEastGlues;
                offsetX = 1;
                offsetY = 0;
                break;
            case SOUTH:
                tstGlues = tst.northGlues;
                glues = openSouthGlues;
                offsetX = 0;
                offsetY = -1;
                break;
            default:
                tstGlues = tst.eastGlues;
                glues = openWestGlues;
                offsetX = -1;
                offsetY = 0;
                break;
        }

        String glue1;
        String glue2;

        for (Coordinate ptPoint : tstGlues.keySet()) {
            for (Coordinate aPoint : glues.keySet()) {
                glue1 = tstGlues.get(ptPoint);
                glue2 = glues.get(aPoint);
                if (tileConfiguration.getStrength(glue1, glue2) > 0 ) {
                    Pair<Coordinate, Coordinate> locAndOffset = getOffset(aPoint, ptPoint, offsetX, offsetY);
                    FrontierElement fe = new FrontierElement(locAndOffset.getKey(), locAndOffset.getValue(), tst);
                    possibleAttach.add(fe);
                    System.out.println("Added one possible tile");
                }
            }
        }
    }

    public Frontier calculateFrontier() {
        ArrayList<FrontierElement> toRemove = new ArrayList<>();
        List<TileSetTile> tilesNoSeed = new ArrayList<>(tileConfiguration.getTileTypes());
        tilesNoSeed.remove(0);
        for (TileSetTile t : tilesNoSeed) {
            checkMatchingGlues(t);
        }
        for (FrontierElement fe : possibleAttach) {
            boolean isStable = calculateStrength(fe.getTileSetTile(), fe.getOffset().getX(), fe.getOffset().getY());
            boolean fitsGeometrically = checkGeometry(fe.getTileSetTile(), fe.getOffset().getX(), fe.getOffset().getY());

            if (isStable && fitsGeometrically) {
                if (!frontier.contains(fe)) {
                    frontier.add(fe);
                }
                toRemove.add(fe);
                System.out.println("add successful");
            }else{
                System.out.println("add failed");
            }
        }
        for (FrontierElement e : toRemove) {
            possibleAttach.remove(e);
            System.out.println("removed from possible tiles");

        }
        return frontier;
    }

    private boolean calculateStrength(TileSetTile p, int x, int y) {
        int totalStrength = 0;

        for (Tile t : p.tiles) {
            String tileGlueN = t.getGlueN();
            if (tileGlueN != null) {
                Coordinate pt = new Coordinate(t.getLocation().getX() + x, t.getLocation().getY() + y + 1);
                Tile nAssemblyTile = Grid.get(pt);
                if (nAssemblyTile != null)
                    totalStrength += tileConfiguration.getStrength(tileGlueN, nAssemblyTile.getGlueS());

            }

            String tileGlueE = t.getGlueE();
            if (tileGlueE != null) {
                Coordinate pt = new Coordinate(t.getLocation().getX() + x + 1, t.getLocation().getY() + y);
                Tile eAssemblyTile = Grid.get(pt);
                if (eAssemblyTile != null)
                    totalStrength += tileConfiguration.getStrength(tileGlueE, eAssemblyTile.getGlueW());

            }

            String tileGlueS = t.getGlueS();
            if (tileGlueS != null) {
                Coordinate pt = new Coordinate(t.getLocation().getX() + x, t.getLocation().getY() + y - 1);
                Tile sAssemblyTile = Grid.get(pt);
                if (sAssemblyTile != null)
                    totalStrength += tileConfiguration.getStrength(tileGlueS, sAssemblyTile.getGlueN());

            }

            String tileGlueW = t.getGlueW();
            if (tileGlueW != null) {
                Coordinate pt = new Coordinate(t.getLocation().getX() + x - 1, t.getLocation().getY() + y);
                Tile wAssemblyTile = Grid.get(pt);
                if (wAssemblyTile != null)
                    totalStrength += tileConfiguration.getStrength(tileGlueW, wAssemblyTile.getGlueE());

            }
        }
        return totalStrength >= tileConfiguration.getTemperature();
    }

    private void attachTile(FrontierElement fe) {
        placeTileSetTile(fe.getTileSetTile(), fe.getOffset().getX(), fe.getOffset().getY());
        frontier.remove(fe);
        cleanUp();
        getOpenGlues();
    }

    public void attachAll() {
        int stop = 0;
        while (!frontier.isEmpty() && stop < numberOfTiles) {
            FrontierElement fe = frontier.get(frontier.randomSelect());
            attachTile(fe);
            calculateFrontier();
            stop++;
        }

    }

    public void placeSeed(TileSetTile t) {
        if (Grid.size() == 0)
            placeTileSetTile(t, 0, 0);
        else
            System.out.println("Grid not empty");
        getOpenGlues();
    }

    public void changeTileConfiguration(TileConfiguration tc) {
        tileConfiguration = tc;
    }

    public TileConfiguration getTileConfiguration(){
        return tileConfiguration;
    }

    public Frontier getFrontier() {
        return frontier;
    }

    public void cleanUp() {
        frontier.clear();
        possibleAttach.clear();
        openNorthGlues.clear();
        openEastGlues.clear();
        openSouthGlues.clear();
        openWestGlues.clear();
    }

    public void setNumberOfTiles(int newNumberOfTiles) {
        numberOfTiles = newNumberOfTiles;
    }

}
