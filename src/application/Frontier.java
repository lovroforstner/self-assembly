package application;

import java.util.ArrayList;
import java.util.Random;

public class Frontier extends ArrayList<FrontierElement> {

    public Frontier() {
        super();
    }
    @Override
    public boolean add(FrontierElement frontierElement) {
        return super.add(frontierElement);
    }

    public int randomSelect() {
        Random rn = new Random();
        return rn.nextInt(this.size());
    }

}
