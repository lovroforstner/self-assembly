package application.configuration;

import application.TileSetTile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TileConfigurationGraphics {
    private final HashMap<Pair<String, String>, Integer> glueFunction = new HashMap<>();
    private final List<TileSetTile> tileTypes = new ArrayList<>();
    private final ObservableList<TileSetTile> observableTileTypes = FXCollections.observableArrayList(tileTypes);

    public TileConfigurationGraphics() {
    }

    public void addGlueFunction(String l1, String l2, Integer s) {
        glueFunction.put(new Pair<>(l1, l2), s);
    }

    public void addTileType(TileSetTile t) {
        if (!observableTileTypes.contains(t))
            observableTileTypes.add(t);
    }

    public HashMap<Pair<String, String>, Integer> getGlueFunction() {
        return glueFunction;
    }

    public ObservableList<TileSetTile> getTileTypes() {
        return observableTileTypes;
    }

}
