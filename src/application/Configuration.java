package application;

public class Configuration {
    public int temparature;
    public long simulationTime;
    public int initialSize;

    public Configuration(int temparature, long simulationTime, int initialSize) {
        this.temparature = temparature;
        this.simulationTime = simulationTime;
        this.initialSize = initialSize;
    }
}

