package application;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TileSetTile {
    public final List<Tile> tiles = new ArrayList<>();

    public final HashMap<Coordinate, String> northGlues = new HashMap<>();
    public final HashMap<Coordinate, String> eastGlues = new HashMap<>();
    public final HashMap<Coordinate, String> southGlues = new HashMap<>();
    public final HashMap<Coordinate, String> westGlues = new HashMap<>();

    private Boolean frontier = false;
    private String tsName;
    public String color;

    public TileSetTile() {
        tsName = "New";
        setColor("002100");
    }

    public TileSetTile(TileSetTile other) {
        tsName = other.tsName;
        color = other.color;
        frontier = other.frontier;
        for (Tile t : other.getTiles()) {
            tiles.add(new Tile(t));
        }
        setGlues();
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void addTile(Tile tile) {
        if (tile.getLocation() != null) {
            if (getTile(tile.getLocation().getX(), tile.getLocation().getY()) == null) {
                tile.setParent(this);
                tiles.add(tile);
                setGlues();
            }
        } else System.out.println("tile does not have a location");
    }

    public Tile getTile(int x, int y) {
        for (Tile tile : tiles) {
            if (tile.getLocation().equals(new Coordinate(x, y))) {
                return tile;
            }
        }
        return null;
    }

    public void setGlues() {
        northGlues.clear();
        eastGlues.clear();
        southGlues.clear();
        westGlues.clear();

        for (Tile t : tiles) {
            String[] glueLabels = t.getGlueLabels();
            if (glueLabels[0] != null) {
                northGlues.put(t.getLocation(), glueLabels[0]);
            }
            if (glueLabels[1] != null) {
                eastGlues.put(t.getLocation(), glueLabels[1]);
            }
            if (glueLabels[2] != null) {
                southGlues.put(t.getLocation(), glueLabels[2]);
            }
            if (glueLabels[3] != null) {
                westGlues.put(t.getLocation(), glueLabels[3]);
            }
        }
    }

    public void setLabel(String label) {
        tsName = label;
    }

    public TileSetTile normalize() {
        TileSetTile copy = new TileSetTile(this);

        Tile min = null;
        for (Tile t : copy.tiles) {
            if (min == null) min = t;
            else {
                if (t.getLocation().getX() <= min.getLocation().getX() && t.getLocation().getY() <= min.getLocation().getY()) {
                    min = t;
                }
            }
        }

        assert min != null;
        int x_translate = -min.getLocation().getX();
        int y_translate = -min.getLocation().getY();

        for (Tile t : copy.tiles) {
            t.setLocation(t.getLocation().translate(x_translate, y_translate));
        }
        return copy;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 13 + (tsName == null ? 0 : tsName.hashCode());
        hash = hash * 17 + tiles.size();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        TileSetTile toCompare = (TileSetTile) obj;
        if (tsName != null && toCompare.getTileSetName() != null) {
            if (!tsName.equals(toCompare.getTileSetName())) return false;
        }


        TileSetTile first = normalize();
        TileSetTile second = toCompare.normalize();

        for (Tile t : first.getTiles()) {
            Coordinate tLoc = t.getLocation();
            Tile t2 = second.getTile(tLoc.getX(), tLoc.getY());
            if (t2 == null) return false;
            else if (!t.equals(t2)) return false;
        }

        for (Tile t : second.getTiles()) {
            Coordinate tLoc = t.getLocation();
            Tile t2 = first.getTile(tLoc.getX(), tLoc.getY());
            if (t2 == null) return false;
            else if (!t.equals(t2)) return false;
        }
        return true;
    }

    public String getTileSetName() {
        return tsName;
    }

}
