package application;

import java.util.Arrays;

public class Tile {

    private String gNorth, gEast, gSouth, gWest;

    private String label = "";

    private Coordinate tileLocation;

    private TileSetTile tileSetTile;

    public Tile(){
    }

    public Tile(int x, int y, TileSetTile parent) {
        gNorth = null;
        gEast= null;
        gSouth = null;
        gWest = null;
        tileSetTile = parent;
        tileLocation = new Coordinate(x, y);
    }

    public Tile(int x, int y, String[] gl, TileSetTile parent) {
        tileLocation = new Coordinate(x,y);
        changeTileGlue(gl);
        tileSetTile = parent;
    }

    public Tile(Tile copy) {
        label = copy.getLabel();
        tileLocation = new Coordinate(copy.getLocation());
        gNorth = copy.getGlueN();
        gEast = copy.getGlueE();
        gSouth = copy.getGlueS();
        gWest = copy.getGlueW();
        tileSetTile = copy.getParent();
    }

    public void setLocation(Coordinate c) {
        tileLocation = new Coordinate(c.getX(), c.getY());
    }

    public Coordinate getLocation() {
        return tileLocation;
    }

    public String getLabel() {
        if (label.isEmpty() && tileSetTile != null) return tileSetTile.getTileSetName();
        else return label;
    }

    public void setLabel(String Label) {
        this.label = Label;
    }

    public String getColor() {
        return tileSetTile.getColor();
    }

    public String getGlueN() {
        return gNorth;
    }

    public void setGlueN(String g) {
        gNorth = g;
    }

    public String getGlueE() {
        return gEast;
    }

    public void setGlueE(String g) {
        gEast = g;
    }

    public String getGlueS() {
        return gSouth;
    }

    public void setGlueS(String g) {
        gSouth = g;
    }

    public String getGlueW() {
        return gWest;
    }

    public void setGlueW(String g) {
        gWest = g;
    }

    public String[] getGlueLabels() {
        String[] glues = new String[4];
        glues[0] = gNorth;
        glues[1] = gEast;
        glues[2] = gSouth;
        glues[3] = gWest;
        return glues;
    }

    public void changeTileGlue(String[] gl) {
        setGlueN(gl[0]);
        setGlueE(gl[1]);
        setGlueS(gl[2]);
        setGlueW(gl[3]);
    }

    public TileSetTile getParent() {
        return tileSetTile;
    }

    public void setParent(TileSetTile p) {
        tileSetTile = p;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Tile)) {
            return false;
        }
        Tile toCompare = (Tile) other;
        if (!tileLocation.equals(toCompare.getLocation()))
            return false;
        return Arrays.deepEquals(toCompare.getGlueLabels(), getGlueLabels());

    }

}
