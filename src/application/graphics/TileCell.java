package application.graphics;

import application.TileSetTile;
import application.graphics.Drawer;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Pos;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;

import java.awt.*;
import java.awt.image.BufferedImage;

public class TileCell extends ListCell<TileSetTile> {
    @Override
    public void updateItem(TileSetTile tile, boolean empty) {
        super.updateItem(tile,empty);
        if (tile != null) {
            BufferedImage iconDrawSpace = new BufferedImage(140, 140, BufferedImage.TYPE_INT_ARGB);
            Graphics2D iconDrawSpaceGraphics = iconDrawSpace.createGraphics();
            iconDrawSpaceGraphics.setClip(0, 0, 140, 140);
            Drawer.TileDrawer.drawGraphicalTile(iconDrawSpaceGraphics, tile);
            WritableImage ptImage = SwingFXUtils.toFXImage(iconDrawSpace, null);
            ImageView view = new ImageView();
            view.setImage(ptImage);
            setAlignment(Pos.CENTER);
            setGraphic(view);
        } else {
            BufferedImage iconDrawSpace = new BufferedImage(140, 140, BufferedImage.TYPE_INT_ARGB);
            Graphics2D iconDrawSpaceGraphics = iconDrawSpace.createGraphics();
            iconDrawSpaceGraphics.setClip(0, 0, 140, 140);
            WritableImage ptImage = SwingFXUtils.toFXImage(iconDrawSpace, null);
            ImageView view = new ImageView();
            view.setImage(ptImage);
            setAlignment(Pos.CENTER);
            setGraphic(view);
        }
    }
}
