package application.graphics;

import application.Coordinate;
import application.Tile;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;

public class SimCanvas extends JPanel {
    private final Dimension res = new Dimension();
    private int tileWidth = 5;
    private Coordinate center;
    private BufferedImage canvasbuff;
    private Graphics2D g2d;

    public SimCanvas(int w, int h) {
        center = new Coordinate(w/2, h/2);
        canvasbuff = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
        g2d = canvasbuff.createGraphics();
        g2d.setComposite(AlphaComposite.Src);
        g2d.setColor(Color.black);
        res.setSize(w,h);
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);
        center = new Coordinate(width / 2, height / 2);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.WHITE);
        canvasbuff.setAccelerationPriority(1);
        g.drawImage(canvasbuff,0,0,null);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1024,768);
    }

    public void drawGrid(HashMap<Coordinate, Tile> hashMap) {
        Drawer.TileDrawer.drawTiles(g2d, hashMap.entrySet(), tileWidth, center);
    }

}