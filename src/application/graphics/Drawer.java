package application.graphics;

import application.Coordinate;
import application.Tile;
import application.TileSetTile;
import javafx.util.Pair;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Drawer {

    public static void clearGraphics(Graphics2D g) {
        Rectangle bounds = g.getClipBounds();
        g.setComposite(AlphaComposite.Clear);
        g.fillRect(0, 0, bounds.width, bounds.height);
        g.setComposite(AlphaComposite.Src);
    }

    public static class TileDrawer {

        public static Coordinate calculateGridDimension(TileSetTile tst) {
            int maxX = Integer.MIN_VALUE;
            int maxY = Integer.MIN_VALUE;
            int minX = Integer.MAX_VALUE;
            int minY = Integer.MAX_VALUE;
            List<Tile> tiles = tst.getTiles();
            for (Tile t : tiles) {
                Coordinate pt = t.getLocation();
                if (pt.getX() > maxX) {
                    maxX = pt.getX();
                }
                if (pt.getX() < minX) {
                    minX = pt.getX();
                }
                if (pt.getY() > maxY) {
                    maxY = pt.getY();
                }
                if (pt.getY() < minY) {
                    minY = pt.getY();
                }
            }
            return new Coordinate(1 + maxX - minX, 1 + maxY - minY);

        }

        public static void drawTile(Graphics2D g, Tile tile, int x, int y, int diameter) {
            g.setFont(g.getFont().deriveFont((float) (diameter / 6)));
            AffineTransform gOriginalATransform = g.getTransform();

            String tileLabel = tile.getLabel();
            String northGlue = tile.getGlueN();
            String eastGlue = tile.getGlueE();
            String southGlue = tile.getGlueS();
            String westGlue = tile.getGlueW();

            Color tileColor = Color.decode(tile.getColor());
            g.setColor(tileColor);

            g.fillRect(x, y, diameter, diameter);

            g.setColor(Color.black);
            g.setStroke(new BasicStroke(diameter / 25));

            FontMetrics font = g.getFontMetrics();

            if (westGlue != null && !westGlue.isEmpty()) {

                g.rotate(Math.PI / 2);
                g.drawString(westGlue, y + (diameter / 2 - font.stringWidth(westGlue) / 2), -x - diameter / 20);
                g.setTransform(gOriginalATransform);
            }
            if (eastGlue != null && !eastGlue.isEmpty()) {
                g.rotate(Math.PI / 2);
                g.drawString(eastGlue, y + (diameter / 2 - font.stringWidth(eastGlue) / 2), -x - diameter + diameter / 6);
                g.setTransform(gOriginalATransform);

            }

            if (southGlue != null && !southGlue.isEmpty()) {
                g.drawString(southGlue, x + diameter / 2 - font.stringWidth(southGlue) / 2, y + diameter - diameter / 20);
            }
            if (northGlue != null && !northGlue.isEmpty()) {
                g.drawString(northGlue, x + diameter / 2 - font.stringWidth(northGlue) / 2, y + diameter / 8);
            }

            g.drawString(tileLabel, x + (diameter / 2) - (font.stringWidth(tileLabel) / 2), y + (diameter / 2));

            g.drawRect(x, y, diameter, diameter);

            g.setStroke(new BasicStroke(diameter / 25));
        }

        public static void drawTiles(Graphics2D g, Set<Map.Entry<Coordinate, Tile>> tiles, int diameter, Coordinate offset) {

            for (Map.Entry<Coordinate, Tile> mep : tiles) {
                Coordinate pt = mep.getKey();
                Tile tile = mep.getValue();
                drawTile(g, tile, pt.getX() * diameter + offset.getX() - diameter / 2, -pt.getY() * diameter + offset.getY() - diameter / 2, diameter);
            }

        }

        public static void drawTileSetTile(Graphics2D g, TileSetTile pt, int diameter, Coordinate offset) {
            Drawer.clearGraphics(g);
            java.util.List<Tile> lt = pt.tiles;
            for (Tile t : lt) {
                drawTile(g, t, t.getLocation().getX() * diameter + offset.getX() - diameter / 2, -t.getLocation().getY() * diameter + offset.getY() - diameter / 2, diameter);
            }

        }

        //Ce bi potreboval za narisati samo en Tile na specificni lokaciji

        public static void drawNewTileSetTile(Graphics2D g, List<Tile> tiles, int diameter, Coordinate location, Coordinate offset) {
            for (Tile t : tiles) {
                drawTile(g, t, (location.getX() + t.getLocation().getX()) * diameter + offset.getX() - diameter / 2, -(location.getY() + t.getLocation().getY()) * diameter + offset.getY() - diameter / 2, diameter);
            }
        }

        public static void drawGraphicalTile(Graphics2D g, TileSetTile pt) {
            Coordinate tstDimension = calculateGridDimension(pt);
            Rectangle graphicsDim = g.getClipBounds();

            int diameter = (int) (Math.ceil(Math.min(graphicsDim.getWidth(), graphicsDim.getHeight()) / Math.max(tstDimension.getX(), tstDimension.getY())) / 1.5);
            Coordinate offset = new Coordinate(0, 0);

            int maxX = Integer.MIN_VALUE;
            int maxY = Integer.MIN_VALUE;
            int minX = Integer.MAX_VALUE;
            int minY = Integer.MAX_VALUE;
            for (Tile t : pt.getTiles()) {
                Coordinate Tpt = t.getLocation();
                maxX = Math.max(Tpt.getX(), maxX);
                maxY = Math.max(Tpt.getY(), maxY);
                minX = Math.min(Tpt.getX(), minX);
                minY = Math.min(Tpt.getY(), minY);
            }

            offset = offset.translate(-minX * diameter + diameter / 2 + ((graphicsDim.width - (tstDimension.getX() * diameter)) / 2), +maxY * diameter + diameter / 2 + ((graphicsDim.height - (tstDimension.getY() * diameter)) / 2));

            drawTileSetTile(g, pt, diameter, offset);
            new Pair<>(offset, diameter);
        }

    }

}
