package application.controllers;
import application.*;
import application.configuration.TileConfiguration;
import application.configuration.TileConfigurationGraphics;
import application.graphics.TileCell;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Pair;
import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;


public class EditorController implements Initializable {

    @FXML
    ListView<TileSetTile> listViewTileset;
    @FXML
    TextField fieldName;
    @FXML
    TextField fieldNorth;
    @FXML
    TextField fieldEast;
    @FXML
    TextField fieldSouth;
    @FXML
    TextField fieldWest;
    @FXML
    TextField numberOfTilesField;
    @FXML
    ColorPicker colorPicker;
    @FXML
    TableView<Glue> glueTable;
    @FXML
    TextField glue1Field;
    @FXML
    TextField glue2Field;
    @FXML
    TextField gluestrengthField;
    @FXML
    Button addglueButton;
    @FXML
    TextField temperatureField;
    @FXML
    Rectangle tileGraphics;
    @FXML
    Text northGraphics;
    @FXML
    Text eastGraphics;
    @FXML
    Text southGraphics;
    @FXML
    Text westGraphics;
    @FXML
    Text nameGraphics;

    private Simulation simulation;
    private TileConfigurationGraphics tcg;
    ObservableList<Glue> glueData = FXCollections.observableArrayList();

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        listViewTileset.setCellFactory(list -> new TileCell());

        TableColumn col1 = glueTable.getColumns().get(0);
        TableColumn col2 = glueTable.getColumns().get(1);
        TableColumn col3 = glueTable.getColumns().get(2);

        col1.setCellValueFactory(new PropertyValueFactory<Glue, String>("glue1"));
        col2.setCellValueFactory(new PropertyValueFactory<Glue, String>("glue2"));
        col3.setCellValueFactory(new PropertyValueFactory<Glue, String>("strength"));
        col1.setCellFactory(TextFieldTableCell.forTableColumn());
        col2.setCellFactory(TextFieldTableCell.forTableColumn());
        col3.setCellFactory(TextFieldTableCell.forTableColumn());

        col1.setOnEditCommit(
                (EventHandler<TableColumn.CellEditEvent<Glue, String>>) t -> {
                    Glue g = t.getTableView().getItems().get(
                            t.getTablePosition().getRow());
                    tcg.getGlueFunction().remove(new Pair<>(g.getGlue1(), g.getGlue2()));
                    g.setGlue1(t.getNewValue());
                    Pair<String, String> p = new Pair<>(g.getGlue1(), g.getGlue2());
                    tcg.getGlueFunction().put(p, Integer.parseInt(g.getStrength()));
                }
        );

        col2.setOnEditCommit(
                (EventHandler<TableColumn.CellEditEvent<Glue, String>>) t -> {
                    Glue g = t.getTableView().getItems().get(
                            t.getTablePosition().getRow());
                    tcg.getGlueFunction().remove(new Pair<>(g.getGlue1(), g.getGlue2()));
                    g.setGlue2(t.getNewValue());
                    Pair<String, String> p = new Pair<>(g.getGlue1(), g.getGlue2());
                    tcg.getGlueFunction().put(p, Integer.parseInt(g.getStrength()));
                }
        );

        col3.setOnEditCommit(
                (EventHandler<TableColumn.CellEditEvent<Glue, String>>) t -> {
                    Glue g = t.getTableView().getItems().get(
                            t.getTablePosition().getRow());
                    tcg.getGlueFunction().remove(new Pair<>(g.getGlue1(), g.getGlue2()));
                    g.setStrength(t.getNewValue());
                    Pair<String, String> p = new Pair<>(g.getGlue1(), g.getGlue2());
                    tcg.getGlueFunction().put(p, Integer.parseInt(g.getStrength()));
                }
        );
        glueTable.setItems(glueData);
        glueTable.setEditable(true);
    }

    public EditorController() {
    }

    public void setTileConfigurationGraphics(TileConfigurationGraphics tcg) {
        this.tcg = tcg;
        if (this.tcg != null) {
            listViewTileset.setItems(tcg.getTileTypes());
            for (Map.Entry<Pair<String, String>, Integer> glueFunct : tcg.getGlueFunction().entrySet()) {
                String glue1 = glueFunct.getKey().getKey();
                String glue2 = glueFunct.getKey().getValue();
                int strength = glueFunct.getValue();
                Glue g = new Glue(glue1, glue2, "" + strength);
                glueData.add(g);
            }
        }
    }

    public void setSimulation(Simulation sim) {
        this.simulation = sim;
    }

    private void setTileData(Tile t) {
        t.setLabel(fieldName.getText().trim());
        t.setGlueN(fieldNorth.getText().trim());
        t.setGlueS(fieldSouth.getText().trim());
        t.setGlueW(fieldWest.getText().trim());
        t.setGlueE(fieldEast.getText().trim());
    }

    public void previewTile() {
        if(TileInfoEmpty()) {
        String N = fieldNorth.getText();
        String E = fieldEast.getText();
        String S = fieldSouth.getText();
        String W = fieldWest.getText();
        String Name = fieldName.getText();

        northGraphics.setText(N);
        eastGraphics.setText(E);
        southGraphics.setText(S);
        westGraphics.setText(W);
        tileGraphics.setFill(colorPicker.getValue());
        nameGraphics.setText(Name);
        }
    }

    public boolean TileInfoEmpty() {
        if ((fieldNorth.getText().isEmpty()) || (fieldEast.getText().isEmpty()) || (fieldSouth.getText().isEmpty()) || (fieldWest.getText().isEmpty())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Not all fields are inserted");
            alert.setContentText("Every side of the tile must be an integer or string and a color must be selected!");
            alert.showAndWait();
            return false;
        } else {
            return true;
        }
    }

    public void addTile() {
        if(TileInfoEmpty()) {
            TileSetTile toAdd = new TileSetTile();
            int x = 0;
            int y = 0;
            Tile t = new Tile(x,y,toAdd);
            setTileData(t);
            toAdd.addTile(t);
            toAdd.setColor("" + toRGBCode(colorPicker.getValue()));
            toAdd.setLabel(fieldName.getText());
            toAdd.setGlues();
            listViewTileset.getItems().add(0, toAdd);
        }
    }

    public void addGlue() {
        String glue1 = glue1Field.getText().trim();
        String glue2 = glue2Field.getText().trim();
        String strength = gluestrengthField.getText().trim();
        if (!glue1.isEmpty() && !glue2.isEmpty()) {
            Glue g = new Glue(glue1, glue2, strength);
            Pair<String, String> p = new Pair<>(g.getGlue1(), g.getGlue2());
            Pair<String, String> p2 = new Pair<>(g.getGlue2(), g.getGlue1());
            if (!tcg.getGlueFunction().containsKey(p) && !tcg.getGlueFunction().containsKey(p2)) {
                glueTable.getItems().add(g);
                tcg.getGlueFunction().put(p, Integer.parseInt(g.getStrength()));
            }
        }

        glue1Field.clear();
        glue2Field.clear();
        gluestrengthField.clear();

    }

    public static class Glue {
        private final SimpleStringProperty glue1;
        private final SimpleStringProperty glue2;
        private final SimpleStringProperty strength;

        private Glue(String g1, String g2, String str) {
            glue1 = new SimpleStringProperty(g1);
            glue2 = new SimpleStringProperty(g2);
            strength = new SimpleStringProperty(str);
        }

        public void setGlue1(String a) {
            glue1.set(a);
        }

        public void setGlue2(String a) {
            glue2.set(a);
        }

        public String getGlue1() {
            return glue1.get();
        }

        public String getGlue2() {
            return glue2.get();
        }

        public String getStrength() {
            return strength.get();
        }

        public void setStrength(String str) {
            try {
                int i = Integer.parseInt(str);
                if (i >= 0) strength.set(str);
            } catch (NumberFormatException ignored) {

            }

        }
    }

    public void setTemperature() {
        simulation.getTileConfiguration().setTemperature(Integer.parseInt(temperatureField.getText()));
    }

    public void updateAssembly() {
        TileConfiguration tc = simulation.getTileConfiguration();
        tc.getTileTypes().clear();
        tc.getGlueFunction().clear();
        tc.loadTileConfigurationGraphics(tcg);
        if (temperatureField.getText().equals("")) {
            simulation.getTileConfiguration().setTemperature(2);
        }else{
            tc.setTemperature(Integer.parseInt(temperatureField.getText()));
        }
        if (numberOfTilesField.getText().equals("")) {
            simulation.assembly.setNumberOfTiles(3000);
        }else {
            setNumberOfTiles();
        }
        FXCollections.reverse(tc.getTileTypes());
        simulation.removeFrontierFromGrid();
        simulation.changeTileConfiguration(tc);
        System.out.println(tc.getGlueFunction());
        System.out.println(simulation.assembly.getTileConfiguration().getTemperature());

    }

    public void setNumberOfTiles() {
        simulation.assembly.numberOfTiles = Integer.parseInt(numberOfTilesField.getText());
    }

    public static String toRGBCode(Color color ) {
        return String.format( "#%02X%02X%02X",
                (int)( color.getRed() * 255 ),
                (int)( color.getGreen() * 255 ),
                (int)( color.getBlue() * 255 ) );
    }

    public void importSierpinski(){
        int x = 0;
        int y = 0;
        simulation.getTileConfiguration().setTemperature(4);

        TileSetTile toAdd = new TileSetTile();
        Tile one = new Tile(x,y,toAdd);
        one.setGlueN("N");
        one.setGlueW("W");
        one.setGlueS("z");
        one.setGlueE("z");
        one.setLabel("SEED");
        toAdd.addTile(one);
        toAdd.setColor(toRGBCode(Color.BLUE));
        toAdd.setGlues();
        listViewTileset.getItems().add(0, toAdd);

        TileSetTile toAdd2 = new TileSetTile();
        Tile two = new Tile(x,y,toAdd2);
        two.setGlueN("N");
        two.setGlueW("1");
        two.setGlueS("N");
        two.setGlueE("z");
        toAdd2.addTile(two);
        toAdd2.setColor(toRGBCode(Color.BLUE));
        toAdd2.setGlues();
        listViewTileset.getItems().add(0, toAdd2);

        TileSetTile toAdd3 = new TileSetTile();
        Tile three = new Tile(x,y,toAdd3);
        three.setGlueN("1");
        three.setGlueW("W");
        three.setGlueS("z");
        three.setGlueE("W");
        toAdd3.addTile(three);
        toAdd3.setColor(toRGBCode(Color.BLUE));
        toAdd3.setGlues();
        listViewTileset.getItems().add(0, toAdd3);

        TileSetTile toAdd4 = new TileSetTile();
        Tile four = new Tile(x,y,toAdd4);
        four.setGlueN("1");
        four.setGlueW("1");
        four.setGlueS("1");
        four.setGlueE("0");
        toAdd4.addTile(four);
        toAdd4.setColor(toRGBCode(Color.BLUE));
        toAdd4.setGlues();
        listViewTileset.getItems().add(0, toAdd4);

        TileSetTile toAdd5 = new TileSetTile();
        Tile five = new Tile(x,y,toAdd5);
        five.setGlueN("1");
        five.setGlueW("1");
        five.setGlueS("0");
        five.setGlueE("1");
        toAdd5.addTile(five);
        toAdd5.setColor(toRGBCode(Color.BLUE));
        toAdd5.setGlues();
        listViewTileset.getItems().add(0, toAdd5);

        TileSetTile toAdd6 = new TileSetTile();
        Tile six = new Tile(x,y,toAdd6);
        six.setGlueN("0");
        six.setGlueW("0");
        six.setGlueS("0");
        six.setGlueE("0");
        toAdd6.addTile(six);
        toAdd6.setColor(toRGBCode(Color.ORANGE));
        toAdd6.setGlues();
        listViewTileset.getItems().add(0, toAdd6);

        TileSetTile toAdd7 = new TileSetTile();
        Tile seven = new Tile(x,y,toAdd7);
        seven.setGlueN("0");
        seven.setGlueW("0");
        seven.setGlueS("1");
        seven.setGlueE("1");
        toAdd7.addTile(seven);
        toAdd7.setColor(toRGBCode(Color.ORANGE));
        toAdd7.setGlues();
        listViewTileset.getItems().add(0, toAdd7);

        String glue1 = "1";
        String glue2 = "1";
        String strength = "2";
        Glue g = new Glue(glue1, glue2, strength);
        Pair<String, String> p = new Pair<>(g.getGlue1(), g.getGlue2());
        Pair<String, String> p2 = new Pair<>(g.getGlue2(), g.getGlue1());
        if (!tcg.getGlueFunction().containsKey(p) && !tcg.getGlueFunction().containsKey(p2)) {
            glueTable.getItems().add(g);
            tcg.getGlueFunction().put(p, Integer.parseInt(g.getStrength()));
        }

        String glue3 = "0";
        String glue4 = "0";
        String strength2 = "2";
        Glue g1 = new Glue(glue3, glue4, strength2);
        Pair<String, String> p3 = new Pair<>(g1.getGlue1(), g1.getGlue2());
        Pair<String, String> p4 = new Pair<>(g1.getGlue2(), g1.getGlue1());
        if (!tcg.getGlueFunction().containsKey(p3) && !tcg.getGlueFunction().containsKey(p4)) {
            glueTable.getItems().add(g1);
            tcg.getGlueFunction().put(p3, Integer.parseInt(g1.getStrength()));
        }

        String glue5 = "N";
        String glue6 = "N";
        String strength3 = "4";
        Glue g2 = new Glue(glue5, glue6, strength3);
        Pair<String, String> p5 = new Pair<>(g2.getGlue1(), g2.getGlue2());
        Pair<String, String> p6 = new Pair<>(g2.getGlue2(), g2.getGlue1());
        if (!tcg.getGlueFunction().containsKey(p5) && !tcg.getGlueFunction().containsKey(p6)) {
            glueTable.getItems().add(g2);
            tcg.getGlueFunction().put(p5, Integer.parseInt(g2.getStrength()));
        }

        String glue7 = "W";
        String glue8 = "W";
        String strength4 = "4";
        Glue g3 = new Glue(glue7, glue8, strength4);
        Pair<String, String> p7 = new Pair<>(g3.getGlue1(), g3.getGlue2());
        Pair<String, String> p8 = new Pair<>(g3.getGlue2(), g3.getGlue1());
        if (!tcg.getGlueFunction().containsKey(p7) && !tcg.getGlueFunction().containsKey(p8)) {
            glueTable.getItems().add(g3);
            tcg.getGlueFunction().put(p7, Integer.parseInt(g3.getStrength()));
        }


    }

    public void importBinary(){
        int x = 0;
        int y = 0;
        simulation.getTileConfiguration().setTemperature(2);

        TileSetTile toAdd = new TileSetTile();
        Tile one = new Tile(x,y,toAdd);
        one.setGlueN("N");
        one.setGlueW("W");
        one.setGlueS("z");
        one.setGlueE("z");
        one.setLabel("SEED");
        toAdd.addTile(one);
        toAdd.setColor(toRGBCode(Color.BLACK));
        toAdd.setGlues();
        listViewTileset.getItems().add(0, toAdd);

        TileSetTile toAdd2 = new TileSetTile();
        Tile two = new Tile(x,y,toAdd2);
        two.setGlueN("N");
        two.setGlueW("1");
        two.setGlueS("N");
        two.setGlueE("z");
        one.setLabel("N");
        toAdd2.addTile(two);
        toAdd2.setColor(toRGBCode(Color.BLACK));
        toAdd2.setGlues();
        listViewTileset.getItems().add(0, toAdd2);

        TileSetTile toAdd3 = new TileSetTile();
        Tile three = new Tile(x,y,toAdd3);
        three.setGlueN("0");
        three.setGlueW("W");
        three.setGlueS("z");
        three.setGlueE("W");
        one.setLabel("W");
        toAdd3.addTile(three);
        toAdd3.setColor(toRGBCode(Color.BLACK));
        toAdd3.setGlues();
        listViewTileset.getItems().add(0, toAdd3);

        TileSetTile toAdd4 = new TileSetTile();
        Tile four = new Tile(x,y,toAdd4);
        four.setGlueN("1");
        four.setGlueW("0");
        four.setGlueS("0");
        four.setGlueE("1");
        one.setLabel("1");
        toAdd4.addTile(four);
        toAdd4.setColor(toRGBCode(Color.BLUE));
        toAdd4.setGlues();
        listViewTileset.getItems().add(0, toAdd4);

        TileSetTile toAdd5 = new TileSetTile();
        Tile five = new Tile(x,y,toAdd5);
        five.setGlueN("1");
        five.setGlueW("0");
        five.setGlueS("1");
        five.setGlueE("0");
        one.setLabel("1");
        toAdd5.addTile(five);
        toAdd5.setColor(toRGBCode(Color.BLUE));
        toAdd5.setGlues();
        listViewTileset.getItems().add(0, toAdd5);

        TileSetTile toAdd6 = new TileSetTile();
        Tile six = new Tile(x,y,toAdd6);
        six.setGlueN("0");
        six.setGlueW("0");
        six.setGlueS("0");
        six.setGlueE("0");
        one.setLabel("0");
        toAdd6.addTile(six);
        toAdd6.setColor(toRGBCode(Color.WHITE));
        toAdd6.setGlues();
        listViewTileset.getItems().add(0, toAdd6);

        TileSetTile toAdd7 = new TileSetTile();
        Tile seven = new Tile(x,y,toAdd7);
        seven.setGlueN("0");
        seven.setGlueW("1");
        seven.setGlueS("1");
        seven.setGlueE("1");
        one.setLabel("0");
        toAdd7.addTile(seven);
        toAdd7.setColor(toRGBCode(Color.WHITE));
        toAdd7.setGlues();
        listViewTileset.getItems().add(0, toAdd7);

        String glue1 = "1";
        String glue2 = "1";
        String strength = "1";
        Glue g = new Glue(glue1, glue2, strength);
        Pair<String, String> p = new Pair<>(g.getGlue1(), g.getGlue2());
        Pair<String, String> p2 = new Pair<>(g.getGlue2(), g.getGlue1());
        if (!tcg.getGlueFunction().containsKey(p) && !tcg.getGlueFunction().containsKey(p2)) {
            glueTable.getItems().add(g);
            tcg.getGlueFunction().put(p, Integer.parseInt(g.getStrength()));
        }

        String glue3 = "0";
        String glue4 = "0";
        String strength2 = "1";
        Glue g1 = new Glue(glue3, glue4, strength2);
        Pair<String, String> p3 = new Pair<>(g1.getGlue1(), g1.getGlue2());
        Pair<String, String> p4 = new Pair<>(g1.getGlue2(), g1.getGlue1());
        if (!tcg.getGlueFunction().containsKey(p3) && !tcg.getGlueFunction().containsKey(p4)) {
            glueTable.getItems().add(g1);
            tcg.getGlueFunction().put(p3, Integer.parseInt(g1.getStrength()));
        }

        String glue5 = "N";
        String glue6 = "N";
        String strength3 = "2";
        Glue g2 = new Glue(glue5, glue6, strength3);
        Pair<String, String> p5 = new Pair<>(g2.getGlue1(), g2.getGlue2());
        Pair<String, String> p6 = new Pair<>(g2.getGlue2(), g2.getGlue1());
        if (!tcg.getGlueFunction().containsKey(p5) && !tcg.getGlueFunction().containsKey(p6)) {
            glueTable.getItems().add(g2);
            tcg.getGlueFunction().put(p5, Integer.parseInt(g2.getStrength()));
        }

        String glue7 = "W";
        String glue8 = "W";
        String strength4 = "2";
        Glue g3 = new Glue(glue7, glue8, strength4);
        Pair<String, String> p7 = new Pair<>(g3.getGlue1(), g3.getGlue2());
        Pair<String, String> p8 = new Pair<>(g3.getGlue2(), g3.getGlue1());
        if (!tcg.getGlueFunction().containsKey(p7) && !tcg.getGlueFunction().containsKey(p8)) {
            glueTable.getItems().add(g3);
            tcg.getGlueFunction().put(p7, Integer.parseInt(g3.getStrength()));
        }


    }


}
