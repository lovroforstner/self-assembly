package application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Controller {
    @FXML
    private Simulation sim;
    @FXML
    private ScrollPane simPane;
    @FXML
    private MenuItem launchEditor;


    public void newAssembly() {
        Assembly asm = new Assembly();
        loadAssembly(asm);
        launchEditor.disableProperty().setValue(false);

    }

    public void loadAssembly(Assembly assembly) {
        final SimCanvas blank = new SimCanvas(1024, 768);
        sim = new Simulation(assembly,blank);
        sim.drawGrid();
        simPane.setContent(sim);
    }

    @FXML
    public void play() {
        sim.play();
    }

    @FXML
    private void launchTileEditor() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/application/TileEditor.fxml"));
        try {
            Parent root1 = loader.load();
            EditorController editorController = loader.getController();
            editorController.setTileConfigurationGraphics(sim.assembly.getTileConfiguration().getTileConfigurationGraphics());
            editorController.setSimulation(sim);
            Stage stage = new Stage();
            stage.setTitle("Tile Editor");
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException ex) {
            Logger.getLogger(EditorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Tukaj bo vzporedno računanje, trenutno se vse najprej izračuna in potem izriše
    /*@FXML
    public void start() {
        final Simulation sim = simulationContent();
        if (sim != null) {
            if (sim.pause) {
                Service<Void> service = new Service<Void>() {
                    @Override
                    protected Task<Void> createTask() {
                        return new Task<Void>() {
                            @Override
                            protected Void call() throws Exception {
                                sim.play();
                                return null;
                            }
                        };
                    }
                    @Override
                    protected void succeeded() {
                        sim.pause = true;
                    }
                };
                service.start();
            } else {
                sim.pause = true;
            }
        }
    }*/


}
