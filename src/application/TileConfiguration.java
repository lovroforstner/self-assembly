package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TileConfiguration {
    private final HashMap<Pair<String, String>, Integer> glueFunction = new HashMap<Pair<String, String>, Integer>();
    private int temperature;
    private final List<TileSetTile> tileTypes = new ArrayList<TileSetTile>();
    private final ObservableList<TileSetTile> observableTileTypes = FXCollections.observableArrayList(tileTypes);

    public TileConfiguration(int temp) {
        temperature = temp;
    }

    public int getStrength(String label1, String label2) {
        Pair<String, String> key1 = new Pair<>(label1, label2);
        Pair<String, String> key2 = new Pair<>(label2, label1);

        try {
            if (glueFunction.containsKey(key1)) {
                return glueFunction.get(key1);
            } else return glueFunction.getOrDefault(key2, 0);
        } catch (NullPointerException npe) {
            return 0;
        }
    }

    public HashMap<Pair<String, String>, Integer> getGlueFunction() {
        return glueFunction;
    }

    public ObservableList<TileSetTile> getTileTypes() {
        return observableTileTypes;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int s) {
        temperature = s;
    }

    public void loadTileConfigurationGraphics(TileConfigurationGraphics tc) {
        if (tc == null)
            return;
        else if (tc.getGlueFunction() == null || tc.getTileTypes() == null) {
            System.out.println("No tile types or no glue function!");
            return;
        }


        glueFunction.putAll(tc.getGlueFunction());

        observableTileTypes.addAll(tc.getTileTypes());

        for (TileSetTile tst : observableTileTypes) {
            tst.setGlues();
        }
        System.out.println("Size after loading tile config " + observableTileTypes.size());

    }

    public TileConfigurationGraphics getTileConfigurationGraphics() {
        TileConfigurationGraphics tcg = new TileConfigurationGraphics();
        for (TileSetTile tileSetTile : observableTileTypes) {
            tcg.addTileType(tileSetTile);
        }

        for (Map.Entry<Pair<String, String>, Integer> glF : glueFunction.entrySet()) {
            String gLabelL = glF.getKey().getKey();
            String gLabelR = glF.getKey().getValue();
            int strength = glF.getValue();
            tcg.addGlueFunction(gLabelL, gLabelR, strength);
        }
        return tcg;
    }


}
