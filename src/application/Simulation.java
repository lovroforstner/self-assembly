package application;
import application.configuration.TileConfiguration;
import application.graphics.SimCanvas;
import javafx.embed.swing.SwingNode;

public class Simulation extends SwingNode {
    public Assembly assembly;
    public Frontier frontier;

    public Simulation() {
        super();
    }

    public Simulation(Assembly assembly, SimCanvas canvas) {
        this.assembly = assembly;
        frontier = assembly.getFrontier();
        setCanvas(canvas);
    }

    public void play() {
        placeSeedInAssembly(assembly.getTileConfiguration().getTileTypes().get(0));
        assembly.attachAll();
        drawGrid();
        System.out.println(frontier.size());
    }

    public void drawGrid() {
        getCanvas().drawGrid(assembly.Grid);
        getCanvas().repaint();
    }

    public void placeSeedInAssembly (TileSetTile tst) {
        frontier.clear();
        assembly.getFrontier().clear();
        assembly.Grid.clear();
        assembly.placeSeed(tst);
        assembly.getOpenGlues();
        frontier = assembly.calculateFrontier();
    }

    public SimCanvas getCanvas() {
        return (SimCanvas) getContent();
    }

    public void setCanvas(SimCanvas sc) {
        setContent(sc);
    }

    public TileConfiguration getTileConfiguration() {
        return assembly.tileConfiguration;
    }

    public void changeTileConfiguration(TileConfiguration tc) {
        assembly.changeTileConfiguration(tc);
    }

    public void removeFrontierFromGrid() {
        for (FrontierElement fe : frontier) {
            if (assembly.Grid.get(fe.getLocation()) == assembly.getTileConfiguration().getTileTypes().get(0).getTile(0,0))
                assembly.Grid.remove(fe.getLocation());
        }
    }



}
